const storage = require('electron-json-storage')
const os = require('os');
const shell = require('electron').shell
const ipc = require('electron').ipcRenderer
// const {app} = require('electron')
const path = require('path');
const fs = require('fs');
const Store = require('electron-store')
const store = new Store()
const SerialPort = require('serialport');
const ByteLength = require('@serialport/parser-byte-length')
const utilsSerial = require('../utilsSerial');
const utilsStorage = require('../utilsStorage');
const knobjs = require('../node_modules/knobjs/knob.min.js')
var $ = require("jquery")
var StringDecoder = require('string_decoder').StringDecoder;
var decoder = new StringDecoder('utf8');

let numMotors = 0
let knobIndexSerial = 0

$(document).ready(function() {

    //Ask for current Store
  let studio = store.get('studio')
  let entireName = studio.song + " - " + studio.name
  //add current preset name
  $(".major > #preset-name").html(entireName)


  //load equipments on screen
  studio.equipment_presets.forEach(function(element, index, array){
    //adiciona amp
    if(element.type == 'amplifier'){
      let brand = element.brand
      var amp_background = '<div style="background-image: url(' + path.join(__dirname, "../assets/images/amps/" + brand + "/fundo.png") + ')" class="image"> <button id="delete-amp" class="button primary" style="float: right;">X</button>'
      var amp_margin = '<div class="amp-margin" style="margin-left:1.4em;">'

      $("#amps-section > .content").append(amp_background);
      $("#amps-section > .content > .image").append(amp_margin);

      divLabel = '<div class="div-label"></div>'
      $("#amps-section > .content > .image > .amp-margin").append(divLabel);

      for (var i = 0; i < 8; i++) {
        var label = '<input type="text" value="' + studio.equipment_presets[0].knobs[i].label.toString() + '" id="knob-label-'+ i +'" class="knob-label" style="margin-top: 30px; margin-left: 12px; background-color: rgba(209,209,209,0.25); width: 70px; display: inline-block; text-align: center; text-transform: uppercase; font-weight: bolder; color: #000;" knob-index="' + i + '" type="amplifier" equipment-index="' + index + '">'
        $("#amps-section > .content > .image > .amp-margin > .div-label").append(label);
      }

      for (var i = 0; i < 8; i++) {
        var amp_knob = '<x-knobjs-knob value='+studio.equipment_presets[0].knobs[i].ang_pos.toString()+' inner-radius="0.4" throw="100" id="amp-knob-' + i + '" index="' + i + '" class="image-knob" onchange="sendValue(this.value, '+i+')"></x-knobjs-knob>'
        $("#amps-section > .content > .image > .amp-margin").append(amp_knob);
        if(studio.equipment_presets[0].knobs[i].motor > 0)
          $("x-knobjs-knob#amp-knob-"+i).addClass("active")
      }

      for (var i = 0; i < 8; i++) {
        //fazer função no utils pra retornar quantidade de amps (countAmp)
        var info = '<label for="amp-motor-' + i + '" style="display: inline-block;">Motor</label><select style="display: inline-block;" class="select-motor" id="amp-motor-' + i + '" knob-index="' + i + '" type="amplifier" equipment-index="' + 0 + '" > <option value="0">0</option> <option value="1">1</option> <option value="2">2</option> <option value="3">3</option> <option value="4">4</option> <option value="5">5</option> <option value="6">6</option> <option value="7">7</option> <option value="8">8</option> </select>'
        $("#amps-section > .content > .image > .amp-margin").append(info);
        $("select#amp-motor-"+i).val(studio.equipment_presets[0].knobs[i].motor)


      }
    //adiciona pedal
    }else if(element.type == 'pedal'){
      $("#pedals-section > .content").append('<a href="#" class="image" pedal-brand="' + element.brand + '" pedal-index="' + index + '" ><img src="../assets/images/pedals/' + element.brand + '/' + element.brand + '.png" alt=""/></a>')
    }
  })

  var amps_menu = '<li><span id="amps-menu" class="opener">AMPLIFIERS</span><ul></ul></li>'
  // var pedals_menu = '<li><span id="pedals-menu" class="opener">PEDALS</span><ul></ul></li>'
  var presets_menu = '<li><span id="presets-menu" class="opener">PRESETS</span><ul></ul></li>'

  $('#sidebar > .inner > #menu > ul').append(amps_menu)
  // $('#sidebar > .inner > #menu > ul').append(pedals_menu)
  $('#sidebar > .inner > #menu > ul').append(presets_menu)

  $('#amps-menu').on('click', function() {
    $(this).toggleClass("active")
  });

  // $('#pedals-menu').on('click', function() {
  //   $(this).toggleClass("active")
  // });

  $('#presets-menu').on('click', function() {
    $(this).toggleClass("active")
  });

  //Reading amplifier's name
  var amps_brand = fs.readdirSync(path.join(__dirname, "../assets/images/amps"))
  amps_brand.forEach(function (element, index, array) {
    var amps_submenu = '<li><a class="amps" href="#" brand=' + element + '>' + element
    $('#amps-menu').parent().children("ul").append(amps_submenu)
  })

  //Reading pedal's name
  var pedals_brand = fs.readdirSync(path.join(__dirname, "../assets/images/pedals"))
  pedals_brand.forEach(function (element, index, array) {
    var pedals_submenu = '<li><a class="pedals" href="#" brand=' + element + '>' + element
    $('#pedals-menu').parent().children("ul").append(pedals_submenu)
  })

  //Reading preset's name (on same song)
  var defaultPath = store.get('roadiebot-path')
  var presets_name = fs.readdirSync(path.join(defaultPath, studio.song))
  presets_name.forEach(function (element, index, array) {
    element = element.split('.').slice(0, -1).join('.')
    var presets_submenu = '<li><a class="presets" href="#" name=' + element + '>' + element
    $('#presets-menu').parent().children("ul").append(presets_submenu)
  })

  $('.amps').on('click', function() {

    let brand = $(this).attr("brand")

    let countAmp = 0
    studio.equipment_presets.forEach(function (element, index, array) {
      if (element.type === 'amplifier') {
        countAmp += 1
      }
    });

    if(countAmp < 1){
      store.set('studio',
        utilsStorage.addDefaultEquip(studio, brand)
      );


      var amp_background = '<div style="background-image: url(' + path.join(__dirname, "../assets/images/amps/" + brand + "/fundo.png") + ')" class="image"> <button id="delete-amp" class="button primary" style="float: right;">X</button>'
      var amp_margin = '<div class="amp-margin" style="margin-left:1.4em;">'

      $("#amps-section > .content").append(amp_background);
      $("#amps-section > .content > .image").append(amp_margin);

      divLabel = '<div class="div-label"></div>'
      $("#amps-section > .content > .image > .amp-margin").append(divLabel);

      for (var i = 0; i < 8; i++) {
        var label = '<input type="text" id="knob-label-'+ i +'" class="knob-label" style="margin-top: 30px; margin-left: 12px; background-color: rgba(209,209,209,0.25); width: 70px; display: inline-block; text-align: center; text-transform: uppercase; font-weight: bolder; color: #000;" knob-index="' + i + '" type="amplifier" equipment-index="' + countAmp + '">'
        $("#amps-section > .content > .image > .amp-margin > .div-label").append(label);
      }

      for (var i = 0; i < 8; i++) {
        var amp_knob = '<x-knobjs-knob value = "0" style="display: inline-block;" inner-radius="0.4" throw="100" id="amp-knob-' + i + '" index="' + i + '" class="image-knob" onchange="sendValue(this.value, '+i+')"></x-knobjs-knob>'
        $("#amps-section > .content > .image > .amp-margin").append(amp_knob);
      }

      for (var i = 0; i < 8; i++) {
        var info = '<label for="amp-motor-' + i + '" style="display: inline-block;">Motor</label><select style="display: inline-block;" class="select-motor" id="amp-motor-' + i + '" knob-index="' + i + '" type="amplifier" equipment-index="' + countAmp + '" > <option value="0" >0</option> <option value="1">1</option> <option value="2">2</option> <option value="3">3</option> <option value="4">4</option> <option value="5">5</option> <option value="6">6</option> <option value="7">7</option> <option value="8">8</option> </select>'
        $("#amps-section > .content > .image > .amp-margin").append(info);
      }

    }

    if(countAmp >= 1){
      alert("Delete an amplifier, please.")
    }

  });

  // $('.pedals').on('click', function() {
  //   let brand = $(this).attr("brand")

  //   let countPedal = 0
  //   studio.equipment_presets.forEach(function (element, index, array) {
  //     if (element.type === 'pedal') {
  //       countPedal += 1
  //     }
  //   });

  //   if(countPedal < 4){
  //     store.set('studio',
  //       utilsStorage.addDefaultEquip(studio, brand)
  //     );
  //     $("#pedals-section > .content").append('<a href="#" class="image" pedal-brand="' + brand + '" pedal-index="' + countPedal + '" ><img src="../assets/images/pedals/' + brand + '/' + brand + '.png") alt=""/></a>')
  //   }

  //   if(countPedal >= 4){
  //     alert("Delete a pedal, please.")
  //   }

  // });

  $('.presets').click(function(){
    let name = $(this).html() + '.json'
    ipc.send('load-preset',name)
  })

  //Header Buttons Listeners
  //
  $('.fa-caret-square-o-right').click(function(){

    if($(this).hasClass("active")){
      $(this).removeClass("active")
    }
    else if(!$(this).hasClass("active")){
      $(this).addClass("active")
      let studio = store.get('studio')
      let {motors, eqIndexes, knobIndexes} = utilsSerial.getMotors(studio)
      motors.forEach(function(element, index){
        let angPos = studio.equipment_presets[eqIndexes[index]].knobs[knobIndexes[index]].ang_pos
        utilsSerial.sendMoveMsg(port, element, angPos)
      })
    }
  })

  $('.fa-save').click(function(){
    ipc.sendSync('save-studio')
  })
  $('.fa-file').click(function(){
    ipc.send('create-preset')
  })
  $('.fa-folder-open').click(function(){
    ipc.send('open-file')
  })

  $('.fa-file').click(function(){
    ipc.send('create-preset')
  })


  //listener dropdown motor
  // dynamically added elements listeners must be like this
  $(document).on('change', ".select-motor", function () {
    let eqIndex = $(this).attr("equipment-index")
    let knobIndex = $(this).attr("knob-index")
    let label = null
    let motor = this.value
    let studio = store.get('studio')
    //update motor on json
    studio = utilsStorage.setMotor(studio, eqIndex, knobIndex, motor)
    store.set('studio', studio)

    //accessing this knob and toggle this active (change color)
    if(motor != "0"){
      $(this).addClass("active")
      let knobIndex = $(this).attr("knob-index")
      let knobId = "amp-knob-" + knobIndex
      $("#amps-section > .content > .image > .amp-margin > #" + knobId).addClass("active")
    }
    if(motor == "0"){
      $(this).removeClass("active")
      let knobIndex = $(this).attr("knob-index")
      let knobId = "amp-knob-" + knobIndex
      $("#amps-section > .content > .image > .amp-margin > #" + knobId).removeClass("active")
    }

  });

  $(document).on('click',"#delete-amp", function() {
    var result = confirm("Deletar?");
    if (result) {
      studio = utilsStorage.deleteEquip(studio,0)
      store.set('studio',studio)
      $('div.image').remove()
    }

  });

  $(document).on('change','.knob-label', function() {
    let studio = store.get('studio')
    let eqIndex = $(this).attr("equipment-index")
    let knobIndex = $(this).attr("knob-index")
    let labelValue = this.value
    studio = utilsStorage.setLabel(studio,eqIndex,knobIndex,labelValue)
    store.set('studio',studio)
    console.log('mudou o knobLabel de número '+knobIndex+' para '+labelValue)
  })

});


function bufferToArray(buffer){
  let array = []
  buffer.forEach(function(element, index, array){
    array.push(element)
  })
  return array
}

//// Serial communication //////////

//setting usb/serial port

portName = store.get('port')


var port = new SerialPort(portName, {
  baudRate: 19200 });



port.on('readable', function(){
  studio = store.get('studio')
  angPos = port.read(numMotors)
    if(angPos != null){
      for(var i = 0; i < numMotors; i++){
        angPosTemp = angPos.toString('ascii').charCodeAt(i).toString(10)
        document.getElementById("amp-knob-"+knobIndexSerial[i]).value = angPosTemp
        store.set('studio', utilsStorage.turnKnob(studio, 0, knobIndexSerial[i], angPosTemp))
      }
    }
})



//sends msg to read all motors
document.getElementsByClassName('fa-refresh')[0].addEventListener('click', (event) => {

  let studio = store.get('studio')
  let {motors, eqIndexes, knobIndexes} = utilsSerial.getMotors(studio)
  numMotors = motors.length
  knobIndexSerial = knobIndexes

  utilsSerial.sendReadMsg(port, motors)
  port.flush()

})
