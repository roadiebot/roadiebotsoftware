const storage = require('electron-json-storage')
const os = require('os');
const shell = require('electron').shell
const ipc = require('electron').ipcRenderer
const path = require('path');
const fs = require('fs');
const utilsStorage = require('../utilsStorage');
const remote = require('electron').remote;


const createDirBtn = document.getElementById('create-directory')
const cancelBtn = document.getElementById('cancel')


createDirBtn.addEventListener('click', function(event){
  event.preventDefault()
  songName = document.getElementById('song-name').value
  presetName = document.getElementById('preset-name').value
  presetObj = utilsStorage.createPresetObj(presetName,songName)
  ipc.send('create-song', presetObj)
})

cancelBtn.addEventListener('click', function(event){
  event.preventDefault()
  var window = remote.getCurrentWindow()
  window.close()
})
