
module.exports = {
  createPresetObj: function(presetName,songName) {
    presetObj = {
      'name': presetName,
      'date': new Date(),
      'song': songName,
      'equipment_presets': []
    }
    return presetObj
  },

  addEquip: function(json, name, type, brand, model, color) {
    var equipment = {
      'name': name,
      'type': type,
      'brand': brand,
      'model': model,
      'color': color,
      'knobs': []
    }
    json.equipment_presets.push(equipment)
    return json
  },

  //add knobQnt of unsetted knobs
  addKnobs: function(json, eqIndex, knobQnt) {
    var knob = {
      'label': ' ',
      'motor': 0,
      'ang_pos': 0,
    }
    for (i = 0; i < knobQnt; i++){
      json.equipment_presets[eqIndex].knobs.push(knob)
    }
    return json
  },

  //add specific knob with specific attributes
  addKnob: function(json, eqIndex, label, motor, ang_pos) {
    var knob = {
      'label': label,
      'motor': motor,
      'ang_pos': ang_pos,
    }
    json.equipment_presets[eqIndex].knobs.push(knob)
    return json
  },

  //update all equipment fields
  updateEquip: function(json, eqIndex, name, type, brand, model, color) {
    if(name)
      json.equipment_presets[eqIndex].name = name
    if(type)
      json.equipment_presets[eqIndex].type = type
    if(brand)
      json.equipment_presets[eqIndex].brand = brand
    if(model)
      json.equipment_presets[eqIndex].model = model
    if(color)
      json.equipment_presets[eqIndex].color = color

    return json
  },

  //update knob label
  setMotor: function(json, eqIndex, knobIndex, motor) {
    if(motor)
      json.equipment_presets[eqIndex].knobs[knobIndex].motor = motor
    return json
  },
  setLabel: function(json, eqIndex, knobIndex, label) {
    if(label)
      json.equipment_presets[eqIndex].knobs[knobIndex].label = label
    return json
  },

  //update
  turnKnob: function(json, eqIndex, knobIndex, ang_pos) {
    if(ang_pos)
      json.equipment_presets[eqIndex].knobs[knobIndex].ang_pos = ang_pos

    return json
  },

  deleteEquip: function(json, eqIndex) {
    json.equipment_presets.splice(eqIndex,1)
    return json
  },

  deleteKnob: function(json, eqIndex, knobIndex) {
    json.equipment_presets[eqIndex].knobs.splice(knobIndex,1)
    return json
  },

  //Add complete default equipment
  addDefaultEquip: function(json, eqString){
    var equipment = {
      'name':  'name',
      'type':  'type',
      'brand': 'brand',
      'model': 'model',
      'color': 'color',
      'knobs': []
    }
    switch(eqString){
      //amps
      case 'fender':
      equipment.name = 'fender'
      equipment.type = 'amplifier'
      equipment.brand = 'fender'
      equipment.model = 'fender'
      equipment.color = 'black'
      json.equipment_presets.push(equipment)
      json = this.addKnobs(json,json.equipment_presets.length - 1 ,8)
      break;

      case 'marshall':
      equipment.name = 'marshall'
      equipment.type = 'amplifier'
      equipment.brand = 'marshall'
      equipment.model = 'marshall'
      equipment.color = 'yellow'
      json.equipment_presets.push(equipment)
      json = this.addKnobs(json,json.equipment_presets.length - 1 ,8)
      break;

      case 'orange':
      equipment.name = 'orange'
      equipment.type = 'amplifier'
      equipment.brand = 'orange'
      equipment.model = 'orange'
      equipment.color = 'orange'
      json.equipment_presets.push(equipment)
      json = this.addKnobs(json,json.equipment_presets.length - 1 ,8)
      break;

      case 'roland':
      equipment.name = 'roland'
      equipment.type = 'amplifier'
      equipment.brand = 'roland'
      equipment.model = 'roland'
      equipment.color = 'blue'
      json.equipment_presets.push(equipment)
      json = this.addKnobs(json,json.equipment_presets.length - 1 ,8)
      break;

      case 'vox':
      equipment.name = 'vox'
      equipment.type = 'amplifier'
      equipment.brand = 'vox'
      equipment.model = 'vox'
      equipment.color = 'red'
      json.equipment_presets.push(equipment)
      json = this.addKnobs(json,json.equipment_presets.length - 1 ,8)
      break;

      //pedais
      case 'pedal1':
      equipment.name = 'pedal1'
      equipment.type = 'pedal'
      equipment.brand = 'pedal1'
      equipment.model = 'pedal1'
      equipment.color = 'undefined'
      json.equipment_presets.push(equipment)
      json = this.addKnobs(json,json.equipment_presets.length - 1 ,1)
      break;

      case 'pedal2':
      equipment.name = 'pedal2'
      equipment.type = 'pedal'
      equipment.brand = 'pedal2'
      equipment.model = 'pedal2'
      equipment.color = 'undefined'
      json.equipment_presets.push(equipment)
      json = this.addKnobs(json,json.equipment_presets.length - 1 ,2)
      break;

      case 'pedal3':
      equipment.name = 'pedal3'
      equipment.type = 'pedal'
      equipment.brand = 'pedal3'
      equipment.model = 'pedal3'
      equipment.color = 'undefined'
      json.equipment_presets.push(equipment)
      json = this.addKnobs(json,json.equipment_presets.length - 1 ,3)
      break;

      case 'pedal4':
      equipment.name = 'pedal4'
      equipment.type = 'pedal'
      equipment.brand = 'pedal4'
      equipment.model = 'pedal4'
      equipment.color = 'undefined'
      json.equipment_presets.push(equipment)
      json = this.addKnobs(json,json.equipment_presets.length - 1 ,4)
      break;

      case 'pedal5':
      equipment.name = 'pedal5'
      equipment.type = 'pedal'
      equipment.brand = 'pedal5'
      equipment.model = 'pedal5'
      equipment.color = 'undefined'
      json.equipment_presets.push(equipment)
      json = this.addKnobs(json,json.equipment_presets.length - 1 ,5)
      break;

      case 'pedal6':
      equipment.name = 'pedal6'
      equipment.type = 'pedal'
      equipment.brand = 'pedal6'
      equipment.model = 'pedal6'
      equipment.color = 'undefined'
      json.equipment_presets.push(equipment)
      json = this.addKnobs(json,json.equipment_presets.length - 1 ,6)
      break;

      case 'pedal8':
      equipment.name = 'pedal8'
      equipment.type = 'pedal'
      equipment.brand = 'pedal8'
      equipment.model = 'pedal8'
      equipment.color = 'undefined'
      json.equipment_presets.push(equipment)
      json = this.addKnobs(json,json.equipment_presets.length - 1 ,8)
      break;
    }
    return json
  },

    //quando for add pedal, tem que mudar essa função para mais equipamentos!!!
  //finds which motors are installed and its angular position
  getMotorsAngPos: function(studio){
    var motors = []
    var angPos = []
    studio.equipment_presets.forEach(function(element, index, array){
      element.knobs.forEach(function(element, index, array){
        if(element.motor !== 0 && element.motor !== '0'){
          motors.push(element.motor)
          angPos.push(element.ang_pos)
        }
      })
    })
    return {motors, angPos}
  }
}


// Objeto de save para referencia
// presetObj = {
// 'name': presetName,
// 'date': new Date(),
// 'song': songName,
// 'equipment_presets': [{
//     'name': 'eq_name',
//     'type': 'eq_type',
//     'brand': 'eq_brand',
//     'model': 'eq_model',
//     'color': 'eq_color',
//     'knobs': [{
//         'label': 'knob_label',
//         'motor': 0,
//         'ang_pos': 0,
//     }]
//   }]
// }
