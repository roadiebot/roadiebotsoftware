// This file is required by the index.html file and will
// be executed in the renderer process for that window.
// All of the Node.js APIs are available in this process.

const {BrowserWindow} = require('electron').remote
const storage = require('electron-json-storage')
const os = require('os');
const shell = require('electron').shell
const ipc = require('electron').ipcRenderer
const editJsonFile = require("edit-json-file")
const fs = require('fs');
const SerialPort = require('serialport');
const Store = require('electron-store')
const store = new Store()
const utilsSerial = require('./utilsSerial');



//Listener do new-preset

document.getElementById('new-preset').addEventListener('click',function(event){
  //gets mother window
  let mainWin = BrowserWindow.getFocusedWindow()

  //creates child window
  let createWin = new BrowserWindow({width: 800, height: 600, modal: true, parent: mainWin, show: false})
  createWin.on('close', () => { win = null })
  createWin.loadFile('screens/create.html')
  // createWin.openDevTools()
  createWin.once('ready-to-show', () => {
    createWin.show()
  })

})

//Listener do open-Project

document.getElementById('open-project').addEventListener('click', function(event){
  ipc.send('open-file')
})


//listener do reconnect
document.getElementById('get-port').addEventListener('click', function(event){
  ipc.send('get-port')
})
//// Serial communication //////////
// finds port automatically //
ipc.send('get-port')
