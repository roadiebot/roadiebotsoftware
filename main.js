// Modules to control application life and create native browser window
const electron = require('electron')

const {app, BrowserWindow, Menu, session} = require('electron')
const ipc = require('electron').ipcMain
const dialog = require('electron').dialog
const fs = require('fs');
const path = require('path');
const Store = require('electron-store') //Usado só como session
const storage = require('electron-json-storage') //Usado para guardar dados
const utilsSerial = require('./utilsSerial')
const utilsStorage = require('./utilsStorage')
const SerialPort = require('serialport');

// Keep a global reference of the window object, if you don't, the window will
// be closed automatically when the JavaScript object is garbage collected.
let mainWindow, studioWindow
const store = new Store()

function createWindow () {
  const {width, height} = electron.screen.getPrimaryDisplay().workAreaSize
  // Create the browser window.
  mainWindow = new BrowserWindow({
    titleBarStyle: 'hidden',
    resizable: false,
    maxWidth: 1200,
    maxHeight: 720,
    width: 1200,
    height: 720,
    show: false,
    icon: path.join(__dirname, 'assets/images/icons/mac/icon.icns')
  })
  mainWindow.setAspectRatio(16/9)
  mainWindow.setFullScreenable(false)
  mainWindow.setResizable(false)

  mainWindow.loadFile('index.html')

  mainWindow.once('ready-to-show', () => {
    mainWindow.show()
  })

  // Open the DevTools.
  // mainWindow.webContents.openDevTools()

  // Emitted when the window is closed.
  mainWindow.on('closed', function () {
    //if(store != storage)
      //dialog('Voce quer salvar?',sim,nao,cancelar)
    // Dereference the window object, usually you would store windows
    // in an array if your app supports multi windows, this is the time
    // when you should delete the corresponding element.
    mainWindow = null
  })


  const template = [
    {
      label: 'File',
      submenu:[
        {
          label: 'New Preset',
          accelerator: 'CmdOrCtrl+N',
          click: function(menuItem, currentWindow){
            var song = store.get('studio.song')
            dialog.showSaveDialog({
              defaultPath: defaultPath + '/' + song,
              buttonLabel: "Save",
              properties: ['openDirectory', 'createDirectory']
            }, function (files) {
              if (files){
                var chosenFilename = files.replace(/^.*[\\\/]/, '')
                let presetObj = utilsStorage.createPresetObj(chosenFilename,song)
                //storage.setDataPath(files)
                storage.set(presetObj.name,presetObj, function(error){
                  if(error)
                    throw error
                  else {
                    //seta studio na store
                    store.set('studio',presetObj)

                    //Main Window loads studio
                    mainWindow.loadFile('screens/studio.html')
                  }
                })

              }
            })
          }

        },
        {
          label: 'Save',
          accelerator: 'CmdOrCtrl+S',
          click: function(menuItem, currentWindow){
            studio = store.get('studio')
            storage.set(studio.name, studio, function(error){
              if(error){
                dialog.showErrorBox('Error','Details: '+error)
                throw error
              } else {
                dialog.showMessageBox({type: 'info', message: 'Saved! Do not move ROADIEBOT-STORAGE FOLDER, please!', buttons: ['Ok']})
              }
            })
          }
        },
        {
          label:'Load',
          accelerator: 'CmdOrCtrl+L',
          click: function(menuItem, currentWindow){
            dialog.showOpenDialog({
              defaultPath: defaultPath,
              properties: ['openFile'],
              filters: [
                {name: 'RoadieBot Presets', extensions: ['json']}
              ]
            }, function (files) {
              if(files){
                var chosenDirectory = path.dirname(files[0])
                var chosenFilename = files[0].replace(/^.*[\\\/]/, '')
                storage.setDataPath(chosenDirectory)
                storage.get(chosenFilename, function(error, data) {
                  if (error) throw error;
                  store.set('studio',data)
                  mainWindow.loadFile('screens/studio.html')
                  // mainWindow.openDevTools()
                })
              }else{

              }

            })
          }
        },
        {
          type:'separator'
        },
        {
          label: 'Exit',
          click(){
            app.quit()
          }
        }
      ]
    },
    {
      label: 'View',
      submenu: [
        {
          label: 'Reload',
          accelerator: 'CmdOrCtrl+R',
          click (item, focusedWindow) {
            if (focusedWindow) focusedWindow.reload()
          }
        },
        {
          label: 'Toggle Developer Tools',
          accelerator: process.platform === 'darwin' ? 'Alt+Command+I' : 'Ctrl+Shift+I',
          click (item, focusedWindow) {
            if (focusedWindow) focusedWindow.webContents.toggleDevTools()
          }
        },
        {
          type: 'separator'
        },
        {
          role: 'resetzoom'
        },
        {
          role: 'zoomin'
        },
        {
          role: 'zoomout'
        },
        {
          type: 'separator'
        },
        {
          role: 'togglefullscreen'
        }
      ]
    },
    {
      role: 'window',
      submenu: [
        {
          role: 'minimize'
        },
        {
          role: 'close'
        }
      ]
    },
    {
      role: 'help',
      submenu: [
        {
          label: 'Learn More',
          click () { require('electron').shell.openExternal('http://electron.atom.io') }
        }
      ]
    }
  ]

  if (process.platform === 'darwin') {
    const name = app.getName()
    template.unshift({
      label: name,
      submenu: [
        {
          role: 'about'
        },
        {
          type: 'separator'
        },
        {
          role: 'services',
          submenu: []
        },
        {
          type: 'separator'
        },
        {
          role: 'hide'
        },
        {
          role: 'hideothers'
        },
        {
          role: 'unhide'
        },
        {
          type: 'separator'
        },
        {
          role: 'quit'
        }
      ]
    })
    // Edit menu.
    template[1].submenu.push(
      {
        type: 'separator'
      },
      {
        label: 'Speech',
        submenu: [
          {
            role: 'startspeaking'
          },
          {
            role: 'stopspeaking'
          }
        ]
      }
    )
    // Window menu.
    template[3].submenu = [
      {
        label: 'Close',
        accelerator: 'CmdOrCtrl+W',
        role: 'close'
      },
      {
        label: 'Minimize',
        accelerator: 'CmdOrCtrl+M',
        role: 'minimize'
      },
      {
        label: 'Zoom',
        role: 'zoom'
      },
      {
        type: 'separator'
      },
      {
        label: 'Bring All to Front',
        role: 'front'
      }
    ]
  }

  var menu = Menu.buildFromTemplate(template)

  Menu.setApplicationMenu(menu)
}

// This method will be called when Electron has finished
// initialization and is ready to create browser windows.
// Some APIs can only be used after this event occurs.
app.on('ready', createWindow)

// Quit when all windows are closed.
app.on('window-all-closed', function () {
  // On OS X it is common for applications and their menu bar
  // to stay active until the user quits explicitly with Cmd + Q
  if (process.platform !== 'darwin') {
    app.quit()
  }
})

app.on('activate', function () {
  // On OS X it's common to re-create a window in the app when the
  // dock icon is clicked and there are no other windows open.
  if (mainWindow === null) {
    createWindow()
  }
})


// In this file you can include the rest of your app's specific main process
// code. You can also put them in separate files and require them here.
//
//

// set default path for song dir
var defaultPath = app.getPath('documents')+"/roadiebot-storage"
store.set('roadiebot-path', defaultPath)
if (fs.existsSync(defaultPath)){}
else  {
  fs.mkdir(defaultPath, function(error){
    if(error) throw error;
  })
}


//Depois ver isso
//app.setPath('songs',defaultPath)



/////////// OPEN A FILE OR DIR ///////////

ipc.on('open-file', function (event) {
  dialog.showOpenDialog({
    defaultPath: defaultPath,
    properties: ['openFile'],
    filters: [
      {name: 'RoadieBot Presets', extensions: ['json']}
    ]
  }, function (files) {
    if(files){
      var chosenDirectory = path.dirname(files[0])
      var chosenFilename = files[0].replace(/^.*[\\\/]/, '')
      storage.setDataPath(chosenDirectory)
      storage.get(chosenFilename, function(error, data) {
        if (error) throw error;
        store.set('studio',data)
        mainWindow.loadFile('screens/studio.html')
        // mainWindow.openDevTools()
      })
    }else{

    }

  })
})

ipc.on('load-preset', function (event, jsonName) {
  storage.get(jsonName, function(error,data){
    if(error) throw error
    store.set('studio',data)
    mainWindow.loadFile('screens/studio.html')
    // mainWindow.openDevTools()
  })
})

/////////// Closing Create Project window and redirecting Index to Studio ///////////

ipc.on('create-song', function (event, presetObj) {
  dialog.showOpenDialog({
    defaultPath: defaultPath,
    buttonLabel: "Save",
    properties: ['openDirectory', 'createDirectory']
  }, function (files) {
    if (files){
      storage.setDataPath(files[0] + '/' + presetObj.song)
      storage.set(presetObj.name,presetObj, function(error){
        if(error)
          throw error
        else {
          //seta studio na store
          store.set('studio',presetObj)

          //pega e fecha a filha do main
          createWindow = mainWindow.getChildWindows()
          createWindow[0].close()

          //Main Window loads studio
          mainWindow.loadFile('screens/studio.html')
        }
      })

    }
  })
})

ipc.on('create-preset', function (event) {
  var song = store.get('studio.song')
  dialog.showSaveDialog({
    defaultPath: defaultPath + '/' + song,
    buttonLabel: "Save",
    properties: ['openDirectory', 'createDirectory']
  }, function (files) {
    if (files){
      var chosenFilename = files.replace(/^.*[\\\/]/, '')
      let presetObj = utilsStorage.createPresetObj(chosenFilename,song)
      //storage.setDataPath(files)
      storage.set(presetObj.name,presetObj, function(error){
        if(error)
          throw error
        else {
          //seta studio na store
          store.set('studio',presetObj)

          //Main Window loads studio
          mainWindow.loadFile('screens/studio.html')
        }
      })

    }
  })
})




//
ipc.on('save-studio',(event) => {
  studio = store.get('studio')
  storage.set(studio.name, studio, function(error){
    if(error){
      dialog.showErrorBox('Error','Details: '+error)
      event.returnValue = false
      throw error
    } else {
      dialog.showMessageBox({type: 'info', message: 'Saved! Do not move ROADIEBOT-STORAGE FOLDER, please!', buttons: ['Ok']})
      event.returnValue = true
    }
  })
})

//Search and save port on store
ipc.on('get-port',(event) => {
  utilsSerial.getSerialPort(SerialPort, store)
  if (!store.get('port'))
    dialog.showErrorBox('Error','DO NOT PROCEED! Verify your USB connection, please.')
})
