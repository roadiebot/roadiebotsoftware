const dialog = require('electron').dialog
// connects automatically to "hardware de interface"
module.exports = {
	getSerialPort: function(SerialPort, store){
		SerialPort.list(function(err, ports) {
		    var allports = ports.length;
		    var count = 0;
		    var done = false
		    ports.forEach(function(port) {
		      count += 1;
		      pm = port['manufacturer'];
		      if (typeof pm !== 'undefined' && pm.includes('Silicon')) {
		        arduinoport = port.comName.toString();
		        store.set('port', arduinoport)
		        done = true;
		      }
		      if (count === allports && done === false) {
						store.set('port', false)
		      }
		    });
		});
	},

	// finds which motors are installed and where they are
	getMotors: function(studio){
		var i
		var j
		var motors = []
		var count = 0
		var eqIndexes = []
		var knobIndexes = []
		for(i = 0; i < studio.equipment_presets.length; i++){
			for(j = 0; j< studio.equipment_presets[i].knobs.length; j++){
				if(studio.equipment_presets[i].knobs[j].motor){
					if (studio.equipment_presets[i].knobs[j].motor !== 0 && studio.equipment_presets[i].knobs[j].motor !== '0'){
						motors[count] = studio.equipment_presets[i].knobs[j].motor
						eqIndexes[count] = i
						knobIndexes[count] = j
						count += 1
					}
				}
			}
		}
		return {motors, eqIndexes, knobIndexes}
	},

	sendCalibrate: function(port){
		let calibrateMsg = 'p' + String.fromCharCode(99)
		port.write(calibrateMsg, function(err) {
		    if (err) {
		      return console.log('Error on write: ', err.message);
		    }
		})
	},

	sendReadMsg: function(port, motors){
		var readMsg = ''
	  motors.forEach(function (element, index, array) {
	  	var motorRead = (50 + +element)
	  	readMsg = readMsg + 'p' + String.fromCharCode(motorRead)
	  })

	  port.write(readMsg, function(err) {
	    if (err) {
	      return console.log('Error on write: ', err.message);
	    }
	  });

	},

	sendMoveMsg: function(port, motor, angPos){
		var moveMsg = 'p' + String.fromCharCode(motor) + 's' + String.fromCharCode(angPos)
		port.write(moveMsg, function(err) {
		  if (err) {
		    return console.log('Error on write: ', err.message);
		  }
		})
	}



}


// Objeto de save para referencia
// presetObj = {
// 'name': presetName,
// 'date': new Date(),
// 'song': songName,
// 'equipment_presets': [{
//     'name': 'eq_name',
//     'type': 'eq_type',
//     'brand': 'eq_brand',
//     'model': 'eq_model',
//     'color': 'eq_color',
//     'knobs': [{
//         'label': 'knob_label',
//         'motor': 0,
//         'ang_pos': 0,
//     }]
//   }]
// }
